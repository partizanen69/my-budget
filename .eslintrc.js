// "eslintConfig": {
//   "extends": [
//     "react-app",
//     "react-app/jest"
//   ]
// },

const commonRules = {
  "prettier/prettier": ["error"],
  "import/prefer-default-export": "off",
  "arrow-body-style": "off",
  "no-console": [
    "warn",
    {
      allow: ["clear", "error"],
    },
  ],
  "class-methods-use-this": "off",
};

module.exports = {
  root: true,
  parser: "@typescript-eslint/parser",
  plugins: ["prettier", "@typescript-eslint"],
  overrides: [
    // backend
    {
      files: ["packages/common/**/*.ts", "packages/server/**/*.ts"],
      extends: ["airbnb-base", "airbnb-typescript/base", "prettier"],
      parserOptions: {
        project: "./tsconfig.eslint.json",
      },
      rules: {
        ...commonRules,
      },
    },
    // front end
    {
      files: ["packages/ui/**/*.ts", "packages/ui/**/*.tsx"],
      extends: ["airbnb", "airbnb-typescript", "prettier"],
      parserOptions: {
        tsconfigRootDir: __dirname,
        project: "./packages/ui/tsconfig.json",
      },
      rules: {
        ...commonRules,
        "react/function-component-definition": [
          "error",
          {
            namedComponents: "arrow-function",
          },
        ],
        "jsx-a11y/label-has-associated-control": "off",
        "import/no-cycle": ["error", { maxDepth: 1 }],
        "no-alert": "off",
      },
    },
  ],
};
