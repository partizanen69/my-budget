# Remove all node_modules (purge)
find . -name node_modules -type d -prune -exec rm -r {} +

# Bootstrap the whole project
"yarn" or "yarn install" from inside root directory

# Add a library to the root package.json
yarn add --dev @types/node -W

# Add a library to the specific package package.json
yarn workspace @my-budget/server add passport
or
yarn workspace @my-budget/server add --dev @types/passport
