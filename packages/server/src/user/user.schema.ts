import { model, ObjectId, Schema } from "mongoose";

export interface User {
  email: string;
  pass: string;
}

export interface UserDoc extends User {
  _id: ObjectId;
}

const userSchema = new Schema<User>({
  email: { type: String, required: true, unique: true },
  pass: { type: String, required: true },
});

userSchema.index({ email: 1 });

export const UserModel = model<User>("User", userSchema);
