import { InsertManyResult } from "mongoose";
import { User, UserDoc, UserModel } from "./user.schema";

export const create = async (user: User): Promise<UserDoc> => {
  const insertManyResult: InsertManyResult<User> = await UserModel.insertMany(
    user,
    {
      rawResult: true,
    }
  );

  return insertManyResult as unknown as UserDoc;
};
