import * as bcrypt from "bcrypt";
import { CreateUserReqBody } from "./user.controller.types";
import * as userRepository from "./user.repository";

export const createUser = async ({
  email,
  pass,
}: CreateUserReqBody): Promise<void> => {
  const salt: string = await bcrypt.genSalt(10);
  const hashedPass: string = await bcrypt.hash(pass, salt);
  await userRepository.create({ email, pass: hashedPass });
};
