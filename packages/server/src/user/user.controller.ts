import { Controller } from "../types";
import { CreateUserReqBody, CreateUserResponse } from "./user.controller.types";
import { createUser } from "./user.service";

export const createUserController: Controller<
  any,
  CreateUserResponse,
  CreateUserReqBody
> = async (req, res, next) => {
  try {
    await createUser(req.body);
  } catch (err) {
    next(err);
    return;
  }

  res.json({ result: true });
};
