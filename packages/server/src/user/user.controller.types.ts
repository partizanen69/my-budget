export type CreateUserResponse = {
  result: true;
};

export type CreateUserReqBody = {
  email: string;
  pass: string;
};
