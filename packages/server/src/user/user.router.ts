import { Router } from "express";
import { createUserController } from "./user.controller";

const userRouter = Router();

userRouter.post("/auth/sign-up", createUserController);

export default userRouter;
