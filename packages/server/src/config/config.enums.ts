export enum EnvVar {
  MONGO_URI = "MONGO_URI",
  MONGO_DBNAME = "MONGO_DBNAME",
  PORT = "PORT",
}
