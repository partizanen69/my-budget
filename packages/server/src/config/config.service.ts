import { config as dotEnvConfig } from "dotenv";
import * as joi from "joi";
import { EnvVar } from "./config.enums";
import { ProcessEnvVars } from "./config.types";

class ConfigService {
  private processEnvVars: ProcessEnvVars;

  constructor() {
    this.processEnvVars = this.initialize();
  }

  private initialize(): ProcessEnvVars {
    dotEnvConfig();

    const envVarsSchema = joi
      .object<ProcessEnvVars>()
      .keys({
        [EnvVar.MONGO_DBNAME]: joi.string().required(),
        [EnvVar.MONGO_URI]: joi.string().required(),
        [EnvVar.PORT]: joi.number().required(),
      })
      .unknown();

    const { value: processEnvVars, error } = envVarsSchema
      .prefs({ errors: { label: "key" } })
      .validate(process.env);

    if (error) {
      throw new Error(
        `Validation of .env file failed because of error: ${error.message}`
      );
    }

    return processEnvVars;
  }

  public get<T extends ProcessEnvVars[EnvVar]>(envVar: EnvVar): T {
    const value = this.processEnvVars[envVar] as T;
    return value;
  }
}

export const configService = new ConfigService();
