import { EnvVar } from "./config.enums";

export type ProcessEnvVars = {
  [EnvVar.MONGO_DBNAME]: string;
  [EnvVar.MONGO_URI]: string;
  [EnvVar.PORT]: number;
};
