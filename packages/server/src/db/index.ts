import mongoose from "mongoose";
import { EnvVar } from "../config/config.enums";
import { configService } from "../config/config.service";
import { logger } from "../logger/logger";

export const connectToDb = async () => {
  const MONGO_URI = configService.get<string>(EnvVar.MONGO_URI);
  const MONGO_DBNAME = configService.get<string>(EnvVar.MONGO_DBNAME);

  logger.info(`mongo URI: ${MONGO_URI}`);

  mongoose.connection.on("error", (err) => {
    logger.error("connection error", err);
  });

  mongoose.connection.on("disconnected", () => {
    logger.info("mongodb disconnected");
  });

  mongoose.connection.on("connected", () => {
    logger.info("mongodb connected");
  });

  logger.info("Trying to establish connection with mongo db...");
  await mongoose.connect(MONGO_URI, {
    dbName: MONGO_DBNAME,
    autoIndex: true,
    minPoolSize: 100,
    serverSelectionTimeoutMS: 1000 * 60 * 60 * 24 * 7, // one week
  });
};
