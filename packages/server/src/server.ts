import * as express from "express";
import * as http from "http";
import * as cors from "cors";
import * as morgan from "morgan";
import * as bodyParser from "body-parser";
// import * as session from "express-session";
// import * as passport from "passport";
// import { Strategy as LocalStrategy, VerifyFunction } from "passport-local";
import userRouter from "./user/user.router";
import { handleError } from "./middlewares/handleError";

const baseUrl = "/api/v1";

export const initializeServer = (): {
  app: express.Application;
  server: http.Server;
} => {
  const app: express.Application = express();

  app.use(
    morgan(
      ":date[iso] :method :url :status :res[content-length] :response-time ms",
      { immediate: false }
    )
  );

  app.use(
    cors({
      origin: "*",
      methods: "GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS",
      credentials: true,
    })
  );

  //   app.use(cookieParser());
  app.use(bodyParser.json({ limit: "25600mb" }));
  app.use(bodyParser.urlencoded({ extended: true }));

  app.use(`${baseUrl}/user`, userRouter);

  app.use(handleError);

  const server = http.createServer(app);

  return {
    app,
    server,
  };
};
