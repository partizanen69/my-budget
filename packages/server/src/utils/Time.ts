export class Time {
  static prepentZeroIfNeeded(num: number): string {
    return num < 10 ? `0${num}` : `${num}`;
  }

  static composeLocalTimeString(date = new Date()): string {
    const year = date.getFullYear();
    const month = this.prepentZeroIfNeeded(date.getMonth() + 1);
    const day = date.getDate();
    const hours = this.prepentZeroIfNeeded(date.getHours());
    const minutes = this.prepentZeroIfNeeded(date.getMinutes());
    const seconds = date.getSeconds();
    const miliSeconds = date.getMilliseconds();

    return `${year}-${month}-${day}T${hours}:${minutes}:${seconds}:${miliSeconds}`;
  }
}
