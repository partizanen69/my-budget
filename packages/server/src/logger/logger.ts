import * as winston from "winston";
import { Time } from "../utils/Time";

const { combine, printf } = winston.format;

const logFormat = printf(
  ({ level, message, ...meta }: winston.Logform.TransformableInfo): string => {
    const timestamp = Time.composeLocalTimeString();

    let finalLogMessage = `${timestamp} [${level}] : ${message}`;
    if (meta.stack) {
      finalLogMessage += ` ${meta.stack}`;
    }

    return finalLogMessage;
  }
);

export const logger = winston.createLogger({
  level: "debug",
  format: combine(
    winston.format.errors({ stack: true }),
    winston.format.colorize(),
    logFormat
  ),
  transports: [new winston.transports.Console()],
});
