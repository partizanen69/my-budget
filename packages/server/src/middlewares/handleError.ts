import httpStatusCodes from "http-status-codes";
import { logger } from "../logger/logger";
import { CustomError, ErrorMiddleware } from "../types";

const httpStatusCodesSet = new Set(Object.values(httpStatusCodes));

// https://stackoverflow.com/a/61464426
// eslint-disable-next-line @typescript-eslint/no-unused-vars
export const handleError: ErrorMiddleware = (err, req, res, next): void => {
  logger.error(req.url);
  logger.error(err);

  let statusCode;
  if (err instanceof CustomError && httpStatusCodesSet.has(err.code)) {
    statusCode = err.code;
  } else {
    statusCode = httpStatusCodes.BAD_REQUEST;
  }

  res.status(statusCode).json({
    error: err.message,
  });
};
