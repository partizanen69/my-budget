import { connectToDb } from "./db";
import { logger } from "./logger/logger";
import { initializeServer } from "./server";

const bootstrapApp = async (): Promise<void> => {
  await connectToDb();

  const { server } = initializeServer();

  const PORT = 8000;
  server.listen(PORT, () => {
    logger.info(`Server is listening at port: ${PORT}`);
  });
};

bootstrapApp()
  .then(() => {
    logger.info(`Application succesfully bootstrapped`);
  })
  .catch((err) => {
    logger.info(`Application bootstrap failed because of error: `, err);
    process.exit(1);
  });
