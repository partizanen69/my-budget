export type AxiosPostResult<T> = AxiosPostSuccess<T> | AxisoResponseErrorData;

export type AxiosPostSuccess<T> = {
  result: T;
};
export type AxisoResponseErrorData = {
  error: string;
};
