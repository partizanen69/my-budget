import { api } from "@my-budget/common";
import { axiosPost, getApiBase } from "../http";
import { SignUpWithLoginAndPassResult } from "./signUp.types";

const authBase = `${getApiBase()}/user/auth`;

export const signUpWithLoginAndPass = async ({
  email,
  pass,
}: {
  email: string;
  pass: string;
}): Promise<SignUpWithLoginAndPassResult> => {
  const endPoint = `${authBase}/sign-up`;

  return axiosPost<api.auth.SignUpResponse>(endPoint, {
    email,
    pass,
  });
};
