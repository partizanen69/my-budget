import { AxisoResponseErrorData } from "../http.types";

export type SignUpWithLoginAndPassResult =
  | AxisoResponseErrorData
  | { result: boolean };
