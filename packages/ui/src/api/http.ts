import Axios, { AxiosError } from "axios";
import { AxiosPostResult } from "./http.types";

export const getApiBase = (): string => {
  return "http://localhost:8000/api/v1";
};

export const axiosPost = async <T>(
  endPoint: string,
  body: Record<string, any>
): Promise<AxiosPostResult<T>> => {
  try {
    const result = await Axios.post<T>(endPoint, body);
    return { result: result.data };
  } catch (err) {
    const error = err as AxiosError<{ error: string }>;

    let errorMsg: string;

    // https://www.npmjs.com/package/axios#handling-errors
    if (error.response) {
      errorMsg = error.response.data.error || error.message;
    } else if (error.request) {
      errorMsg = "The request was made but no response was received";
    } else {
      errorMsg = error.message;
    }

    return { error: errorMsg };
  }
};
