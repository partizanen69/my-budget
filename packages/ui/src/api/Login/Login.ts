import { api } from "@my-budget/common";
import { axiosPost, getApiBase } from "../http";

const authBase = `${getApiBase()}/user/auth`;

export const loginWithLoginAndPass = ({
  email,
  pass,
}: {
  email: string;
  pass: string;
}) => {
  const endPoint = `${authBase}/login`;

  return axiosPost<api.auth.LoginResponse>(endPoint, {
    email,
    pass,
  });
};
