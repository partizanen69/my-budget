import React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { Header } from "./components/Header/Header";
import { Home } from "./pages/home/Home";
import { Login } from "./pages/login/Login";
import { SignUp } from "./pages/signup/SignUp";
import "antd/dist/antd.min.css";
import styles from "./App.module.scss";
import { Pages } from "./App.types";

const App = () => {
  return (
    <>
      <div className={styles.header}>
        <Header />
      </div>
      <div className={styles.main}>
        <BrowserRouter>
          <Routes>
            <Route path={Pages.home} element={<Home />} />
            <Route path={Pages.login} element={<Login />} />
            <Route path={Pages.signUp} element={<SignUp />} />
          </Routes>
        </BrowserRouter>
      </div>
    </>
  );
};

export default App;
