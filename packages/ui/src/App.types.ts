export enum Pages {
  home = "/",
  login = "/login",
  signUp = "/sign-up",
}
