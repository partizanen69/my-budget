import { useCallback, useState } from "react";

export const useLogin = () => {
  const [email, setEmail] = useState<string>("");
  const [pass, setPass] = useState<string>("");
  const [loginInProgress, setLoginInProgress] = useState<boolean>(false);

  const handleSubmit = useCallback((): void => {
    if (!email || !pass) {
      alert("Please enter login and password");
      return;
    }

    setLoginInProgress(true);

    setLoginInProgress(false);
  }, [email, pass]);

  return {
    email,
    setEmail,
    pass,
    setPass,
    loginInProgress,
    handleSubmit,
  };
};
