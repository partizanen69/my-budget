import { Button, Input } from "antd";
import React from "react";
import cn from "classnames";
import styles from "./Login.module.scss";
import { useLogin } from "./useLogin";

export const Login = () => {
  const { email, setEmail, pass, setPass, loginInProgress, handleSubmit } =
    useLogin();

  return (
    <div className={styles.login}>
      <div>Log in</div>
      <div className={styles.formwWrap}>
        <div className={styles.formControl}>
          <label>Email</label>
          <Input
            id="email"
            size="large"
            value={email}
            onChange={(e) => setEmail(e.target.value.trim())}
            disabled={loginInProgress}
          />
        </div>
        <div className={styles.formControl}>
          <label>Password</label>
          <Input
            id="pass"
            size="large"
            value={pass}
            onChange={(e) => setPass(e.target.value.trim())}
            disabled={loginInProgress}
          />
        </div>
        <div className={cn(styles.formControl, styles.submitBtn)}>
          <Button
            type="primary"
            htmlType="submit"
            loading={loginInProgress}
            onClick={handleSubmit}
          >
            Sign Up
          </Button>
        </div>
      </div>
    </div>
  );
};
