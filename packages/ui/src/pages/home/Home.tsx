import React from "react";
import { Link } from "react-router-dom";

export const Home = () => {
  return (
    <>
      <Link to="/login">Login</Link>
      <Link to="/sign-up">Sign Up</Link>
    </>
  );
};
