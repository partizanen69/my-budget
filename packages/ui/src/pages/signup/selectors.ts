import { RootDispatch, RootState } from "../../store";

export const selectDispatch = ({ signup }: RootDispatch) => ({
  doSomething: signup.doSomething,
});

export const selectState = ({ signup }: RootState) => ({
  email: signup.email,
  // pass: signup.pass,
  // confirmPass: signup.confirmPass,
});
