import { useCallback, useMemo, useState } from "react";
import { useNavigate } from "react-router-dom";
import { signUpWithLoginAndPass } from "../../api/SignUp/signUp";
import { Pages } from "../../App.types";

export const useSignUp = () => {
  // const [email, setEmail] = useState<string>("");
  const [email, setEmail] = useState<string>("barmaleys877@gmail.com");
  // const [pass, setPass] = useState<string>("");
  const [pass, setPass] = useState<string>("sdsd#DDaa");
  // const [confirmPass, setConfirmPass] = useState<string>("");
  const [confirmPass, setConfirmPass] = useState<string>("sdsd#DDaa");

  const [isEmailValid, setIsEmailValid] = useState<boolean>(false);
  const [isPassValid, setIsPassValid] = useState<boolean>(false);
  const [isConfirmPassValid, setIsCofnirmPassValid] = useState<boolean>(false);
  const [signUpInProgress, setSignUpInProgress] = useState<boolean>(false);

  const navigate = useNavigate();

  const signUpContextProps = useMemo(
    () => ({
      email,
      setEmail,
      pass,
      setPass,
      confirmPass,
      setConfirmPass,
      isEmailValid,
      setIsEmailValid,
      isPassValid,
      setIsPassValid,
      isConfirmPassValid,
      setIsCofnirmPassValid,
      signUpInProgress,
    }),
    [
      email,
      pass,
      confirmPass,
      isEmailValid,
      isPassValid,
      isConfirmPassValid,
      signUpInProgress,
    ]
  );

  const handleSubmit = useCallback(async (): Promise<void> => {
    if (!isEmailValid || !isPassValid || !isConfirmPassValid) {
      alert("Some of validation rules did not pass");
      return;
    }

    if (pass !== confirmPass) {
      alert('Values in "Password" and "Confirm password" are not the same');
      return;
    }

    setSignUpInProgress(true);
    const result = await signUpWithLoginAndPass({ email, pass });
    setSignUpInProgress(false);

    if ("error" in result) {
      alert(`Sign up failed because of the reason: ${result.error}`);
      return;
    }

    alert("Sign up success");
    navigate(Pages.login);
  }, [email, isEmailValid, pass, isPassValid, confirmPass, isConfirmPassValid]);

  return {
    signUpContextProps,
    signUpInProgress,
    handleSubmit,
  };
};
