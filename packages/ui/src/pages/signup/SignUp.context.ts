import { createContext } from "react";
import { SignUpContextType } from "./SignUp.types";

const defaultContextValue = {} as SignUpContextType;
export const SignUpContext =
  createContext<SignUpContextType>(defaultContextValue);
