import { useContext } from "react";
import { SignUpContext } from "../../SignUp.context";

export const usePassInputWithPopover = () => {
  const { signUpInProgress } = useContext(SignUpContext);
  return { signUpInProgress };
};
