import React from "react";
import { Input, Popover } from "antd";
import PassPopover from "../PassPopover/PassPopover";
import { usePassInputWithPopover } from "./usePassInputWithPopover";

export const PassInputWithPopover = ({
  passInputValue,
  passInputOnchangeCallback,
}: {
  passInputValue: string;
  passInputOnchangeCallback: React.Dispatch<React.SetStateAction<string>>;
}) => {
  const { signUpInProgress } = usePassInputWithPopover();

  return (
    <Popover
      placement="right"
      trigger="focus"
      content={<PassPopover passInputValue={passInputValue} />}
    >
      <Input
        size="large"
        value={passInputValue}
        onChange={(e) => passInputOnchangeCallback(e.target.value)}
        disabled={signUpInProgress}
      />
    </Popover>
  );
};
