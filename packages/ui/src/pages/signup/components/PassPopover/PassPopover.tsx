import React, { memo } from "react";
import { IsValidIcon } from "../IsValidIcon/IsValidIcon";
import styles from "./PassPopover.module.scss";
import { usePassPopover } from "./usePassPopover";

const PassPopover = ({ passInputValue }: { passInputValue: string }) => {
  const {
    containsBigLetters,
    containsSmallLetters,
    containsSpecialChars,
    longEnough,
  } = usePassPopover({ passInputValue });

  return (
    <div className={styles.main}>
      <div>
        <IsValidIcon isValid={containsSmallLetters} />
        <span>Must contain small latin letters</span>
      </div>
      <div>
        <IsValidIcon isValid={containsBigLetters} />
        <span>Must contain big latin letters</span>
      </div>
      <div>
        <IsValidIcon isValid={containsSpecialChars} />
        <span>Must contain special characters: ?=.*[!@#$%^&*]\</span>
      </div>
      <div>
        <IsValidIcon isValid={longEnough} />
        <span>Must be at least 8 characters long</span>
      </div>
    </div>
  );
};

export default memo(PassPopover);
