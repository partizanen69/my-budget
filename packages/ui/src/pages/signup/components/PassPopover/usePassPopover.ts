import { useContext, useEffect, useMemo } from "react";
import { SignUpContext } from "../../SignUp.context";

export const usePassPopover = ({
  passInputValue,
}: {
  passInputValue: string;
}) => {
  const { pass, setIsPassValid, confirmPass, setIsCofnirmPassValid } =
    useContext(SignUpContext);

  const {
    containsSmallLetters,
    containsBigLetters,
    longEnough,
    containsSpecialChars,
    valid,
  } = useMemo(() => {
    const specialCharacters: string[] = [
      "?",
      "=",
      ".",
      "*",
      "[",
      "!",
      "@",
      "#",
      "$",
      "%",
      "^",
      "&",
      "*",
      "]",
      `\\`,
    ];
    const regExp = new RegExp(`[${specialCharacters.join("\\")}]`);

    const isContainsSmallLetters: boolean = /[a-z]/.test(passInputValue);
    const isContainsBigLetters: boolean = /[A-Z]/.test(passInputValue);
    const isContainsSpecialChars: boolean = regExp.test(passInputValue);
    const isLongEnough: boolean = passInputValue.length >= 8;

    const isValid: boolean =
      isContainsSmallLetters &&
      isContainsBigLetters &&
      isContainsSpecialChars &&
      isLongEnough;

    return {
      containsSmallLetters: isContainsSmallLetters,
      containsBigLetters: isContainsBigLetters,
      longEnough: isLongEnough,
      containsSpecialChars: isContainsSpecialChars,
      valid: isValid,
    };
  }, [passInputValue]);

  useEffect(() => {
    if (passInputValue === pass) {
      setIsPassValid(valid);
    }
  }, [valid, pass]);

  useEffect(() => {
    if (passInputValue === confirmPass) {
      setIsCofnirmPassValid(valid);
    }
  }, [valid, confirmPass]);

  return {
    containsBigLetters,
    containsSmallLetters,
    containsSpecialChars,
    longEnough,
  };
};
