import { useContext, useEffect } from "react";
import { SignUpContext } from "../../SignUp.context";

export const useEmailPopover = () => {
  const { email, setIsEmailValid } = useContext(SignUpContext);
  const isEmailValid: boolean = /^\S+@\S+\.\S+$/.test(email);
  useEffect(() => {
    setIsEmailValid(isEmailValid);
  }, [isEmailValid]);

  return { isEmailValid };
};
