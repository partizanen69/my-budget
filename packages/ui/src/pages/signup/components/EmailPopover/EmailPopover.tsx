import React from "react";
import { IsValidIcon } from "../IsValidIcon/IsValidIcon";
import { useEmailPopover } from "./useEmailPopover";

export const EmailPopover = () => {
  const { isEmailValid } = useEmailPopover();

  return (
    <>
      <IsValidIcon isValid={isEmailValid} />
      <span>
        {isEmailValid
          ? "It looks like a valid email"
          : "It doesn't look like an email"}
      </span>
    </>
  );
};
