import React from "react";
import cn from "classnames";
import CloseOutlined from "@ant-design/icons/CloseOutlined";
import CheckOutlined from "@ant-design/icons/CheckOutlined";
import styles from "./IsValidIcon.module.scss";

export const IsValidIcon = ({ isValid }: { isValid: boolean }) => {
  return isValid ? (
    <CheckOutlined className={cn(styles.icon, styles.valid)} />
  ) : (
    <CloseOutlined className={cn(styles.icon, styles.invalid)} />
  );
};
