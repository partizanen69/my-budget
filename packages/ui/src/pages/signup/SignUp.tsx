import React, { FC } from "react";
import cn from "classnames";
import { Input, Button, Popover } from "antd";
import styles from "./SignUp.module.scss";
import { useSignUp } from "./useSignUp";
import { EmailPopover } from "./components/EmailPopover/EmailPopover";
import { SignUpContext } from "./SignUp.context";
import { PassInputWithPopover } from "./components/PassInputWithPopover/PassInputWithPopover";

export const SignUp: FC = () => {
  const { signUpContextProps, signUpInProgress, handleSubmit } = useSignUp();
  const { email, setEmail, pass, setPass, confirmPass, setConfirmPass } =
    signUpContextProps;

  return (
    <SignUpContext.Provider value={signUpContextProps}>
      <div className={styles.signUp}>
        <div className={styles.formwWrap}>
          <div className={styles.formControl}>
            <label>Email:</label>
            <Popover
              placement="right"
              trigger="focus"
              content={<EmailPopover />}
            >
              <Input
                id="email"
                size="large"
                value={email}
                onChange={(e) => setEmail(e.target.value.trim())}
                disabled={signUpInProgress}
              />
            </Popover>
          </div>
          <div className={styles.formControl}>
            <label>Password:</label>
            <PassInputWithPopover
              passInputValue={pass}
              passInputOnchangeCallback={setPass}
            />
          </div>
          <div className={styles.formControl}>
            <label>Confirm password:</label>
            <PassInputWithPopover
              passInputValue={confirmPass}
              passInputOnchangeCallback={setConfirmPass}
            />
          </div>
          <div className={cn(styles.formControl, styles.submitBtn)}>
            <Button
              type="primary"
              htmlType="submit"
              loading={signUpInProgress}
              onClick={handleSubmit}
            >
              Sign Up
            </Button>
          </div>
        </div>
      </div>
    </SignUpContext.Provider>
  );
};
