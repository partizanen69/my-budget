export type SignUpForm = {
  email: string;
  password: string;
  confirmPassword: string;
};

export type SignUpContextType = {
  email: string;
  setEmail: React.Dispatch<React.SetStateAction<string>>;
  pass: string;
  setPass: React.Dispatch<React.SetStateAction<string>>;
  confirmPass: string;
  setConfirmPass: React.Dispatch<React.SetStateAction<string>>;
  isEmailValid: boolean;
  setIsEmailValid: React.Dispatch<React.SetStateAction<boolean>>;
  isPassValid: boolean;
  setIsPassValid: React.Dispatch<React.SetStateAction<boolean>>;
  isConfirmPassValid: boolean;
  setIsCofnirmPassValid: React.Dispatch<React.SetStateAction<boolean>>;
  signUpInProgress: boolean;
};
