import { init, RematchDispatch, RematchRootState } from "@rematch/core";
// import { connectRouter, routerMiddleware } from 'connected-react-router';
// import { history } from './browser-history';

import models, { RootModel } from "./models";

// export { models };
// eslint-disable-next-line @typescript-eslint/naming-convention
// export type models = typeof models;

export type RootState = RematchRootState<RootModel>;
export type RootDispatch = RematchDispatch<RootModel>;

const store = init({
  models,
  // redux: {
  // reducers: {
  //   router: connectRouter(history),
  // },
  // middlewares: [routerMiddleware(history)],
  // },
});

export default store;
