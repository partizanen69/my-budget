import { Models } from "@rematch/core";
import signUpModel from "./signup/signup.model";
import { SignUpModel } from "./signup/signup.model.types";

export interface RootModel extends Models<RootModel> {
  signup: SignUpModel;
}

const models: RootModel = {
  signup: signUpModel,
};

export default models;
