export interface SignUpState {
  email: string;
}

export interface SignUpModel {
  state: SignUpState;
  reducers: {
    updateState: (
      state: SignUpState,
      payload: Partial<SignUpState>
    ) => SignUpState;
  };
  effects: {
    doSomething: (param: string) => void;
  };
}
