import { SignUpModel } from "./signup.model.types";

const signUpModel: SignUpModel = {
  state: {
    email: "",
  },
  reducers: {
    updateState: (state, payload) => {
      return { ...state, payload };
    },
  },
  effects: {
    doSomething: () => {},
  },
};

export default signUpModel;
